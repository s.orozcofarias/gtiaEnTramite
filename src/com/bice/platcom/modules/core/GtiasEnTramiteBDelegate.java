package com.bice.platcom.modules.core;

import org.apache.turbine.util.db.pool.DBConnection;

import com.bice.fogape.mantenedor.soporte.LogDinamico;
import com.bice.platcom.modules.bean.StatusBean;
import com.bice.platcom.modules.manager.GtiasEnTramiteManager;


public class GtiasEnTramiteBDelegate {

	GtiasEnTramiteManager gtiasEnTramiteManager = new GtiasEnTramiteManager(null);
	
public GtiasEnTramiteBDelegate(){
	
}

public StatusBean ConsultaValorUfDolar(){
	
	StatusBean status = new StatusBean();
	
	status = gtiasEnTramiteManager.ConsultaValorUfDolar();		
	
	return status;
	
}

public StatusBean ConsultaGtiaEnTramite(String numIden, String numEvento, String filtro){
	
	StatusBean status = new StatusBean();
	
	status = gtiasEnTramiteManager.ConsultaGtiaEnTramite(numIden,numEvento, filtro);		
	
	return status;
	
}


public StatusBean ConsultaEventosGtiaEnTramite(long numEvento, String filtro){
	
	StatusBean status = new StatusBean();
	
	status = gtiasEnTramiteManager.ConsultaEventosGtiaEnTramite(numEvento, filtro);		
	
	return status;
	
}


public StatusBean consultaSiHayDocumentos(long numGarantia, long numEvento, String filtro){

	StatusBean status = new StatusBean();
	
	status = gtiasEnTramiteManager.ConsultaDocumentosGtiaEnTramite(numGarantia, numEvento, filtro);		
	
	return status;

	
}


public GtiasEnTramiteManager getGtiasEnTramiteManager() {
	return gtiasEnTramiteManager;
}

public void setGtiasEnTramiteManager(GtiasEnTramiteManager gtiasEnTramiteManager) {
	this.gtiasEnTramiteManager = gtiasEnTramiteManager;
}


public DBConnection getConexion(){

	return  gtiasEnTramiteManager.getConexionDb();

	
}


}

