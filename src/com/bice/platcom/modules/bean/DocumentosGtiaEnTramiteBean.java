package com.bice.platcom.modules.bean;

import java.sql.Blob; 

public class DocumentosGtiaEnTramiteBean {
	
	private int numGarantia;
	private int folio;
	private String fecha;
	private String glosa;
	private String tipo;
	private Blob binFile;	
	private String fileName;
	

	
	public int getNumGarantia() {
		return numGarantia;
	}



	public void setNumGarantia(int numGarantia) {
		this.numGarantia = numGarantia;
	}



	public int getFolio() {
		return folio;
	}



	public void setFolio(int folio) {
		this.folio = folio;
	}



	public String getFecha() {
		return fecha;
	}



	public void setFecha(String fecha) {
		this.fecha = fecha;
	}



	public String getGlosa() {
		return glosa;
	}



	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}



	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public Blob getBinFile() {
		return binFile;
	}



	public void setBinFile(Blob binFile) {
		this.binFile = binFile;
	}



	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	@Override
	public String toString() {
	
		return "(" + numGarantia +", "+ folio +", "+ fecha +", "+ glosa +", "+ tipo 
				+", "+ binFile +", "+ fileName +" )"; 
	}
	
	

}
