package com.bice.platcom.modules.bean;

import java.util.Date;

public class GtiasEnTramiteBean {

	private long numSecuencia;
	private String rutCliente;
	private long numGarantia;
	private long numEvento;
	private String codTipGtia;
	private String glsTipGtia;
	private String descripGtia;
	private String codTipEve;
	private String glsTipEve;
	private String CodTramAct;
	private String GlsTramAct;
	private String CodEstAct;
	private String GlsEstAct;
	private String FecIngreso;
	private String TotalDias;
	private Date fecTramAct;
		
	
	
	public long getNumGarantia() {
		return numGarantia;
	}
	public void setNumGarantia(long numGarantia) {
		this.numGarantia = numGarantia;
	}
	public long getNumEvento() {
		return numEvento;
	}
	public void setNumEvento(long numEvento) {
		this.numEvento = numEvento;
	}
	public String getCodTipGtia() {
		return codTipGtia;
	}
	public void setCodTipGtia(String codTipGtia) {
		this.codTipGtia = codTipGtia;
	}
	public String getGlsTipGtia() {
		return glsTipGtia;
	}
	public void setGlsTipGtia(String glsTipGtia) {
		this.glsTipGtia = glsTipGtia;
	}
	public String getDescripGtia() {
		return descripGtia;
	}
	public void setDescripGtia(String descripGtia) {
		this.descripGtia = descripGtia;
	}
	
	public String getCodTipEve() {
		return codTipEve;
	}
	public void setCodTipEve(String codTipEve) {
		this.codTipEve = codTipEve;
	}
	public String getGlsTipEve() {
		return glsTipEve;
	}
	public void setGlsTipEve(String glsTipEve) {
		this.glsTipEve = glsTipEve;
	}
	public String getCodTramAct() {
		return CodTramAct;
	}
	public void setCodTramAct(String codTramAct) {
		CodTramAct = codTramAct;
	}
	public String getGlsTramAct() {
		return GlsTramAct;
	}
	public void setGlsTramAct(String glsTramAct) {
		GlsTramAct = glsTramAct;
	}
	public String getCodEstAct() {
		return CodEstAct;
	}
	public void setCodEstAct(String codEstAct) {
		CodEstAct = codEstAct;
	}
	public String getGlsEstAct() {
		return GlsEstAct;
	}
	public void setGlsEstAct(String glsEstAct) {
		GlsEstAct = glsEstAct;
	}

	public String getFecIngreso() {
		return FecIngreso;
	}
	public void setFecIngreso(String fecIngreso) {
		FecIngreso = fecIngreso;
	}
	
	public String getTotalDias() {
		return TotalDias;
	}
	public void setTotalDias(String totalDias) {
		TotalDias = totalDias;
	}
	
	public long getNumSecuencia() {
		return numSecuencia;
	}
	public void setNumSecuencia(long numSecuencia) {
		this.numSecuencia = numSecuencia;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public Date getFecTramAct() {
		return fecTramAct;
	}
	public void setFecTramAct(Date fecTramAct) {
		this.fecTramAct = fecTramAct;
	}
	
	
	
	
	
}
