package com.bice.platcom.modules.bean;

import java.sql.Date;

public class EventosGtiaEnTramiteBean {

	private int numSecuencia;
	private int numEvento;
	private int numTramite;
	private String codTramite;
	private String glsTramite;
	private int numSecTram;
	private String codTipGar;
	private String codTipEve;
	private String fecIniEve;
	private String fecFinEve;
	private String glsDurEtapa;
	private String glsDurTram;
	private String codRolEtapa;
	private String numRolSLA;
	private String glsSLAOcupado;
	private String flgRolSupSLA;
	private int numEje;
	private String nomEje;
	private String codEje;
	private String codEstado;
	private String glsEstado;
	private String flgObs;
	private String GlsObs;
	private String flgUltEve;
	private String flgSuperaSLA;
	private String flgTramTermi;
	private String glsCssDest;
	private String glsCssLetras;
	
	private String rolEtapa;
	private String glsRolEtapa;
	private String numDiasSLA;
	private String numDiasRest;
	
	public int getNumSecuencia() {
		return numSecuencia;
	}
	public void setNumSecuencia(int numSecuencia) {
		this.numSecuencia = numSecuencia;
	}
	public int getNumEvento() {
		return numEvento;
	}
	public void setNumEvento(int numEvento) {
		this.numEvento = numEvento;
	}
	
	public String getGlsTramite() {
		return glsTramite;
	}
	public void setGlsTramite(String glsTramite) {
		this.glsTramite = glsTramite;
	}
	public int getNumSecTram() {
		return numSecTram;
	}
	public void setNumSecTram(int numSecTram) {
		this.numSecTram = numSecTram;
	}
	public String getFecIniEve() {
		return fecIniEve;
	}
	public void setFecIniEve(String fecIniEve) {
		this.fecIniEve = fecIniEve;
	}
	public String getFecFinEve() {
		return fecFinEve;
	}
	public void setFecFinEve(String fecFinEve) {
		this.fecFinEve = fecFinEve;
	}
	
	public String getGlsDurEtapa() {
		return glsDurEtapa;
	}
	public void setGlsDurEtapa(String glsDurEtapa) {
		this.glsDurEtapa = glsDurEtapa;
	}
	public String getGlsDurTram() {
		return glsDurTram;
	}
	public void setGlsDurTram(String glsDurTram) {
		this.glsDurTram = glsDurTram;
	}
	public String getCodRolEtapa() {
		return codRolEtapa;
	}
	public void setCodRolEtapa(String codRolEtapa) {
		this.codRolEtapa = codRolEtapa;
	}
	public String getNumRolSLA() {
		return numRolSLA;
	}
	public void setNumRolSLA(String numRolSLA) {
		this.numRolSLA = numRolSLA;
	}
	public String getGlsSLAOcupado() {
		return glsSLAOcupado;
	}
	public void setGlsSLAOcupado(String glsSLAOcupado) {
		this.glsSLAOcupado = glsSLAOcupado;
	}
	public String getFlgRolSupSLA() {
		return flgRolSupSLA;
	}
	public void setFlgRolSupSLA(String flgRolSupSLA) {
		this.flgRolSupSLA = flgRolSupSLA;
	}
	public int getNumEje() {
		return numEje;
	}
	public void setNumEje(int numEje) {
		this.numEje = numEje;
	}
	public String getNomEje() {
		return nomEje;
	}
	public void setNomEje(String nomEje) {
		this.nomEje = nomEje;
	}
	public String getCodEje() {
		return codEje;
	}
	public void setCodEje(String codEje) {
		this.codEje = codEje;
	}
	
	public String getGlsEstado() {
		return glsEstado;
	}
	public void setGlsEstado(String glsEstado) {
		this.glsEstado = glsEstado;
	}
	public String getFlgObs() {
		return flgObs;
	}
	public void setFlgObs(String flgObs) {
		this.flgObs = flgObs;
	}
	
	public String getFlgUltEve() {
		return flgUltEve;
	}
	public void setFlgUltEve(String flgUltEve) {
		this.flgUltEve = flgUltEve;
	}
	public String getFlgSuperaSLA() {
		return flgSuperaSLA;
	}
	public void setFlgSuperaSLA(String flgSuperaSLA) {
		this.flgSuperaSLA = flgSuperaSLA;
	}
	public int getNumTramite() {
		return numTramite;
	}
	public void setNumTramite(int numTramite) {
		this.numTramite = numTramite;
	}
	public String getCodTipGar() {
		return codTipGar;
	}
	public void setCodTipGar(String codTipGar) {
		this.codTipGar = codTipGar;
	}
	public String getCodTipEve() {
		return codTipEve;
	}
	public void setCodTipEve(String codTipEve) {
		this.codTipEve = codTipEve;
	}
	
	public String getFlgTramTermi() {
		return flgTramTermi;
	}
	public void setFlgTramTermi(String flgTramTermi) {
		this.flgTramTermi = flgTramTermi;
	}
	public String getGlsCssDest() {
		return glsCssDest;
	}
	public void setGlsCssDest(String glsCssDest) {
		this.glsCssDest = glsCssDest;
	}
	public String getGlsCssLetras() {
		return glsCssLetras;
	}
	public void setGlsCssLetras(String glsCssLetras) {
		this.glsCssLetras = glsCssLetras;
	}
	public String getCodTramite() {
		return codTramite;
	}
	public void setCodTramite(String codTramite) {
		this.codTramite = codTramite;
	}
	public String getCodEstado() {
		return codEstado;
	}
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
	}
	public String getGlsObs() {
		return GlsObs;
	}
	public void setGlsObs(String glsObs) {
		GlsObs = glsObs;
	}
	public String getRolEtapa() {
		return rolEtapa;
	}
	public void setRolEtapa(String rolEtapa) {
		this.rolEtapa = rolEtapa;
	}
	public String getNumDiasSLA() {
		return numDiasSLA;
	}
	public void setNumDiasSLA(String numDiasSLA) {
		this.numDiasSLA = numDiasSLA;
	}
	public String getNumDiasRest() {
		return numDiasRest;
	}
	public void setNumDiasRest(String numDiasRest) {
		this.numDiasRest = numDiasRest;
	}
	public String getGlsRolEtapa() {
		return glsRolEtapa;
	}
	public void setGlsRolEtapa(String glsRolEtapa) {
		this.glsRolEtapa = glsRolEtapa;
	}

	
		
}

