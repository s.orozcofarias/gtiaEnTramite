package com.bice.platcom.modules.screens.garantias;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.sonda.bancos.SONDAframe.modules.screens.SONDAframeScreen;
import com.bice.platcom.modules.bean.GtiasEnTramiteBean;
import com.bice.platcom.modules.bean.StatusBean;
import com.bice.platcom.modules.core.GtiasEnTramiteBDelegate;
import com.bice.platcom.modules.util.Constants;
import com.bice.fogape.mantenedor.soporte.LogDinamico;

public class PCO_Frm_GtiasEnTramite extends SONDAframeScreen {

	private String ValUF;
	private String ValDolar;
	private String MensajePag;
	private List GtiasEnTramiteList = null;
	private List EventosGtiaEnTramiteList = null;
	private List GtiaEnTramitelinkList = null;
	private List EventosGtiaEnTramiteResSLAList = null;
	
	StatusBean ValMon = new StatusBean();
	StatusBean GtiasEnTramite = new StatusBean();
	StatusBean EventosGtiaEnTramite = new StatusBean();
	
	GtiasEnTramiteBDelegate gtiasEnTramiteBDelegate = new GtiasEnTramiteBDelegate();
	GtiasEnTramiteBean GtiaEnTramitelink = new GtiasEnTramiteBean();
	PCO_Detail_Documents pantallaDocumentos = new PCO_Detail_Documents();		
	
	
	
	public PCO_Frm_GtiasEnTramite(){
		
    }
	
	@Override
	public void doBuildTemplate(RunData data, Context context) throws Exception {		
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.doBuildTemplate() ");
		super.doBuildTemplate(data, context);
		
		//DATOS ENTREGADO POR PARAM
		String numIden = data.getParameters().getString(Constants.VALUE_RUT," "); 
		String name = data.getParameters().getString(Constants.VALUE_NAME," ");
		String filtro = data.getParameters().getString(Constants.VALUE_FILTRO," ");
		LogDinamico.debug("numIden: " +numIden);
		LogDinamico.debug("filtro: " +filtro);
		
		ValMon = gtiasEnTramiteBDelegate.ConsultaValorUfDolar();
		if(ValMon.getStatus()){
				this.setValDolar(ValMon.getValdolar());
				this.setValUF(ValMon.getValUF());	
			}
									
		GtiasEnTramite = gtiasEnTramiteBDelegate.ConsultaGtiaEnTramite(numIden,null,filtro);
		if(GtiasEnTramite.getStatus()){
			this.setGtiasEnTramiteList((List) GtiasEnTramite.getObjResul());
			this.setMensajePag(GtiasEnTramite.getMensaje());
		}
		
		//DATOS SETEADOS EN C0NTEXTO
		context.put("MENSAJE_PAG", this.getMensajePag());
		context.put("VALOR_DOLAR", this.getValDolar());
		context.put("VALOR_UF",this.getValUF()); 
		context.put("GTIAS_TRAMITE",this.getGtiasEnTramiteList()); 
		context.put("FLG_DET_EVENTO_GTIA", "N");
		
		//DATOS RESCATOS DESDE TABLA: GARANTIAS EN TRAMITE
		String tipoAccion = data.getParameters().getString(Constants.VALUE_TIPO_ACCION," "); //ACCION A EJECUTAR
		String numGtiaStr = data.getParameters().getString(Constants.VALUE_GARANTIA," "); //NUM GARANTIA ENTREGADA 
		String numEventoStr = data.getParameters().getString(Constants.VALUE_EVENTO," ");
		String tipGtia = data.getParameters().getString(Constants.VALUE_TIP_GARANTIA," "); 
		String desGtia = data.getParameters().getString(Constants.VALUE_DES_GARANTIA," ");  
		String tipEvenGtia = data.getParameters().getString(Constants.VALUE_TIP_EVE_GTIA," "); 
		String tramActGtia = data.getParameters().getString(Constants.VALUE_TRAM_ACT_GTIA," "); 
		String estGtia = data.getParameters().getString(Constants.VALUE_EST_ACT," "); 
		String fecIngreso = data.getParameters().getString(Constants.VALUE_FEC_INGRESO," "); 
		String totDias = data.getParameters().getString(Constants.VALUE_TOT_DIAS," "); 
		
		if(tipoAccion.equals("DETALLE_EVENTO_GARANTIA")){
			LogDinamico.debug("tipoAccion--> DETALLE_EVENTO_GARANTIA");
			GtiaEnTramitelinkList = (List) new ArrayList(); 
			
			GtiaEnTramitelink.setNumGarantia(Long.parseLong(numGtiaStr));
			GtiaEnTramitelink.setNumEvento(Long.parseLong(numEventoStr));
			GtiaEnTramitelink.setGlsTipGtia(tipGtia);
			GtiaEnTramitelink.setDescripGtia(desGtia);
			GtiaEnTramitelink.setGlsTipEve(tipEvenGtia);
			GtiaEnTramitelink.setGlsTramAct(tramActGtia);
			GtiaEnTramitelink.setGlsEstAct(estGtia);
			GtiaEnTramitelink.setFecIngreso(fecIngreso);
			GtiaEnTramitelink.setTotalDias(totDias);
			
			//se agregan campos a la lista
			GtiaEnTramitelinkList.add(GtiaEnTramitelink);
			//se setea lista en contexto
			context.put("GTIA_TRAMITE_LINK",GtiaEnTramitelinkList); 
			
			EventosGtiaEnTramite = gtiasEnTramiteBDelegate.ConsultaEventosGtiaEnTramite(Long.parseLong(numEventoStr),filtro);
			if(EventosGtiaEnTramite.getStatus()){
				LogDinamico.debug("EventosGtiaEnTramite.getStatus()--> TRUE");
				this.setEventosGtiaEnTramiteList((List) EventosGtiaEnTramite.getObjResul());
				context.put("FLG_DET_EVENTO_GTIA", "S");
				context.put("EVENTOS_GTIA_TRAMITE",this.getEventosGtiaEnTramiteList()); 
				
				this.setEventosGtiaEnTramiteResSLAList((List) EventosGtiaEnTramite.getObjResul2());
				context.put("EVENTOS_GTIA_TRAMITE_RES_SLA",this.getEventosGtiaEnTramiteResSLAList()); 
				context.put("NUM_GAR", numGtiaStr);
				context.put("NUM_EVENTO", numEventoStr);
				context.put("TIPO_FILTRO", filtro);
				
			}
						
		}
		
	}


	public String getValUF() {
		return ValUF;
	}

	public void setValUF(String valUF) {
		ValUF = valUF;
	}

	public String getValDolar() {
		return ValDolar;
	}

	public void setValDolar(String valDolar) {
		ValDolar = valDolar;
	}

	
	public GtiasEnTramiteBDelegate getGtiasEnTramiteBDelegate() {
		return gtiasEnTramiteBDelegate;
	}

	public void setGtiasEnTramiteBDelegate(
			GtiasEnTramiteBDelegate gtiasEnTramiteBDelegate) {
		this.gtiasEnTramiteBDelegate = gtiasEnTramiteBDelegate;
	}

	public List getGtiasEnTramiteList() {
		return GtiasEnTramiteList;
	}

	public void setGtiasEnTramiteList(List gtiasEnTramiteList) {
		GtiasEnTramiteList = gtiasEnTramiteList;
	}

	public List getEventosGtiaEnTramiteList() {
		return EventosGtiaEnTramiteList;
	}

	public void setEventosGtiaEnTramiteList(List eventosGtiaEnTramiteList) {
		EventosGtiaEnTramiteList = eventosGtiaEnTramiteList;
	}

	public List getGtiaEnTramitelinkList() {
		return GtiaEnTramitelinkList;
	}

	public void setGtiaEnTramitelinkList(List gtiaEnTramitelinkList) {
		GtiaEnTramitelinkList = gtiaEnTramitelinkList;
	}

	public GtiasEnTramiteBean getGtiaEnTramitelink() {
		return GtiaEnTramitelink;
	}

	public void setGtiaEnTramitelink(GtiasEnTramiteBean gtiaEnTramitelink) {
		GtiaEnTramitelink = gtiaEnTramitelink;
	}

	public String getMensajePag() {
		return MensajePag;
	}

	public void setMensajePag(String mensajePag) {
		MensajePag = mensajePag;
	}

	public List getEventosGtiaEnTramiteResSLAList() {
		return EventosGtiaEnTramiteResSLAList;
	}

	public void setEventosGtiaEnTramiteResSLAList(
			List eventosGtiaEnTramiteResSLAList) {
		EventosGtiaEnTramiteResSLAList = eventosGtiaEnTramiteResSLAList;
	}
	
	

}



