package com.bice.platcom.modules.util;

public class Constants {

	//Conexiones BD
	public static final String DB_PRV = "prv";
    public static final String DB_GRT = "grt";
    
  //Campos Presentacion CONSULTA  desde el VM 
    //------------------------------------------------------------------------
    public static final String VALUE_RUT = "PARAM-RUT";
    public static final String VALUE_NAME = "PARAM-NAME";
    public static final String VALUE_FILTRO = "PARAM-FILTRO";
    public static final String VALUE_GARANTIA = "LINK_NUM_GARANTIA";
    public static final String VALUE_EVENTO = "LINK_NUM_EVENTO";
    public static final String VALUE_TIPO_ACCION = "VALUE_TIPO_ACCION";
    public static final String VALUE_TIP_GARANTIA = "LINK_TIP_GARANTIA";
    public static final String VALUE_DES_GARANTIA = "LINK_DES_GARANTIA";
    public static final String VALUE_TIP_EVE_GTIA = "LINK_TIP_EVE_GTIA";
    public static final String VALUE_TRAM_ACT_GTIA = "LINK_TRAM_ACT_GTIA";
    public static final String VALUE_EST_ACT = "LINK_EST_ACT";
    public static final String VALUE_FEC_INGRESO = "LINK_FEC_INGRESO";
    public static final String VALUE_TOT_DIAS = "LINK_TOT_DIAS";    
    
    }

