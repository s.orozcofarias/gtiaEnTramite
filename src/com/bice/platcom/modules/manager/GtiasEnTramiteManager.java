package com.bice.platcom.modules.manager;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleTypes;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.db.pool.DBConnection;
import org.apache.velocity.context.Context;

import com.aditiva.common.biceutil.core.DataNew;
import com.aditiva.common.biceutil.core.Key;
import com.bice.fogape.mantenedor.soporte.LogDinamico;
import com.bice.platcom.modules.util.Constants;
import com.bice.platcom.modules.bean.DocumentosGtiaEnTramiteBean;
import com.bice.platcom.modules.bean.EventosGtiaEnTramiteBean;
import com.bice.platcom.modules.bean.GtiasEnTramiteBean;
import com.bice.platcom.modules.bean.StatusBean;
import com.bice.provisiones.modules.core.*;

public class GtiasEnTramiteManager  extends PCBase { 


	public GtiasEnTramiteManager(String spid){
		super(spid);
	}
	
	public StatusBean  ConsultaValorUfDolar() {
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaValorUfDolar()");
		DBConnection dbConnPrv = null;
		CallableStatement cs  = null;
		StatusBean status = new StatusBean();

		try {
			dbConnPrv = BasePeer.beginTransaction(Constants.DB_GRT);			
			String sp = "{call GRT_ADMIN.GRT_SP_CON_VALDOLYUF(?,?,?,?)}"; 
			
			LogDinamico.debug("sp : " + sp);
			cs = dbConnPrv.getConnection().prepareCall(sp);
			
			cs.registerOutParameter(1, Types.NUMERIC);
			cs.registerOutParameter(2, Types.NUMERIC);
			cs.registerOutParameter(3, Types.NUMERIC);
			cs.registerOutParameter(4, Types.VARCHAR);
			
			cs.executeQuery();
			
			if (cs.getInt(3) == 0) {
				LogDinamico.debug("ConsultaValorUfDolar() ");
				status.setValdolar(cs.getString(1));
				status.setValUF(cs.getString(2));
				status.setStatus(true);
			} else {
				LogDinamico.debug("ERROR : " + cs.getInt(4));
			}
			
			BasePeer.commitTransaction(dbConnPrv);			
		} catch (Exception e) {
			LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaValorUfDolar: execute --> Error al consultar: " + e.getMessage());
			e.printStackTrace();
			status.setStatus(false);
			try {
				BasePeer.rollBackTransaction(dbConnPrv);
			} catch (Exception ex) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaValorUfDolar:, error en rollback : " + ex.getMessage());
				status.setStatus(false);
			}

		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				
				if(dbConnPrv != null){
					dbConnPrv.close();
				}
			} catch (SQLException e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaValorUfDolar:, error en dbConnPrv close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			} catch (Exception e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaValorUfDolar:, error en dbConnPrv close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			}
		}
		return status;
	}
	
	
	public StatusBean  ConsultaGtiaEnTramite(String numIden, String numEvento, String filtro) {
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite, numIden: " +numIden);
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite, numIden: " +filtro);
		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List gtiaEnTramiteList  = null;
		StatusBean status = new StatusBean();
		status.setMensaje("");
		
		//saco digito al rut
		String numIdenSinDV = numIden.substring(0, numIden.length()-1);
		LogDinamico.debug("numIdenSinDV : " +numIdenSinDV);
		
		try {
			dbConnGrt = BasePeer.beginTransaction(Constants.DB_GRT);			
			String sp = "{call GRT_ADMIN.GRT_SP_LIS_RESGARTRAM(?,?,?,?,?,?)}";
			
			LogDinamico.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);
			
			cs.setString(1, numIdenSinDV);
			cs.setInt(2, 0);
			cs.setString(3, filtro);
			
			cs.registerOutParameter(4, Types.NUMERIC);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR );
			
			cs.executeQuery();
						
			if (cs.getInt(4) == 0) {
				LogDinamico.debug("ConsultaGtiaEnTramite() EXITOSO ");
				rs = (ResultSet)  cs.getObject(6);
				gtiaEnTramiteList = (List) new ArrayList();   

				 while (rs.next()) {
                    
					 GtiasEnTramiteBean gtiasEnTramiteBean = new GtiasEnTramiteBean();
					 
					 gtiasEnTramiteBean.setNumSecuencia(rs.getInt(1));
					 gtiasEnTramiteBean.setRutCliente(rs.getString(2));
					 gtiasEnTramiteBean.setNumEvento(rs.getInt(3));
					 gtiasEnTramiteBean.setNumGarantia(rs.getInt(4));
					 gtiasEnTramiteBean.setCodTipGtia(rs.getString(5));
					 gtiasEnTramiteBean.setGlsTipGtia(rs.getString(6));
					 gtiasEnTramiteBean.setDescripGtia(rs.getString(7));
					 gtiasEnTramiteBean.setCodTipEve(rs.getString(8));
					 gtiasEnTramiteBean.setGlsTipEve(rs.getString(9));
					 gtiasEnTramiteBean.setCodTramAct(rs.getString(10));
					 gtiasEnTramiteBean.setGlsTramAct(rs.getString(11));
					 gtiasEnTramiteBean.setCodEstAct(rs.getString(12));
					 gtiasEnTramiteBean.setGlsEstAct(rs.getString(13));
					 gtiasEnTramiteBean.setFecIngreso(rs.getString(14));
					 gtiasEnTramiteBean.setTotalDias(rs.getString(15));
					 gtiasEnTramiteBean.setFecTramAct(rs.getDate(16));
					 
					 
					 
					 gtiaEnTramiteList.add(gtiasEnTramiteBean);//inserta datos en lista

				 }
				 LogDinamico.debug("Garantias en tramite: " +  gtiaEnTramiteList.size());
				 	if ( gtiaEnTramiteList.size()==0){
				 		LogDinamico.debug("No se encontraron registros garantias en tramite" );
				 		status.setMensaje("No existen Garantias en tramite");
				 		
					}
				 	
				 	status.setStatus(true);
					status.setObjResul(gtiaEnTramiteList);
 	
				
			} else {
				LogDinamico.debug("ConsultaGtiaEnTramite() ERROR : " + cs.getString(4));
				status.setStatus(false);
			}
			
			BasePeer.commitTransaction(dbConnGrt);
									
		} catch (Exception e) {
			LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite: execute --> Error al consultar: " + e.getMessage());
			e.printStackTrace();
			status.setStatus(false);			
			try {
				BasePeer.rollBackTransaction(dbConnGrt);
			} catch (Exception ex) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite:, error en rollback : " + ex.getMessage());
				status.setStatus(false);
			}
			
			
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				
				if(dbConnGrt != null){
					dbConnGrt.close();
				}
			} catch (SQLException e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			} catch (Exception e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			}
		}
		return status;
	}
	
	public StatusBean  ConsultaEventosGtiaEnTramite(long numEvento, String filtro) {
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite, numEvento: " +numEvento);
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite, filtro: " +filtro);
		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List eventosGtiaEnTramiteList  = null;
		List eventosGtiaEnTramiteResSLAList = null;
		StatusBean status = new StatusBean();
		
		try {
			dbConnGrt = BasePeer.beginTransaction(Constants.DB_GRT);			
			String sp = "{call GRT_ADMIN.GRT_SP_LIS_EVEGARTRAM(?,?,?,?,?,?)}"; 
			
			LogDinamico.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);
			
			cs.setLong(1, numEvento);
			cs.setString(2, filtro);
			
			cs.registerOutParameter(3, Types.NUMERIC);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.registerOutParameter(5, OracleTypes.CURSOR );
			cs.registerOutParameter(6, OracleTypes.CURSOR );
			
			cs.executeQuery();
						
			if (cs.getInt(3) == 0) {
				LogDinamico.debug("ConsultaEventosGtiaEnTramite() EXITOSO ");
				rs = (ResultSet)  cs.getObject(5);
				eventosGtiaEnTramiteList = (List) new ArrayList();   

				 while (rs.next()) {
                    
					 EventosGtiaEnTramiteBean eventosGtiaEnTramiteBean = new EventosGtiaEnTramiteBean();
					 
					 
					 eventosGtiaEnTramiteBean.setNumSecuencia(rs.getInt(1));
					 eventosGtiaEnTramiteBean.setNumEvento(rs.getInt(2));
					 eventosGtiaEnTramiteBean.setNumTramite(rs.getInt(3));
					 eventosGtiaEnTramiteBean.setCodTramite(rs.getString(4));
					 eventosGtiaEnTramiteBean.setGlsTramite(rs.getString(5));
					 eventosGtiaEnTramiteBean.setNumSecTram(rs.getInt(6));
					 eventosGtiaEnTramiteBean.setCodTipGar(rs.getString(7));
					 eventosGtiaEnTramiteBean.setCodTipEve(rs.getString(8));
					 eventosGtiaEnTramiteBean.setFecIniEve(rs.getString(9));
					 eventosGtiaEnTramiteBean.setFecFinEve(rs.getString(10));
					 eventosGtiaEnTramiteBean.setGlsDurEtapa(rs.getString(11));
					 eventosGtiaEnTramiteBean.setGlsDurTram(rs.getString(12));
					 eventosGtiaEnTramiteBean.setCodRolEtapa(rs.getString(13));
					 eventosGtiaEnTramiteBean.setNumRolSLA(rs.getString(14));
					 eventosGtiaEnTramiteBean.setGlsSLAOcupado(rs.getString(15));
					 eventosGtiaEnTramiteBean.setFlgRolSupSLA(rs.getString(16));
					 eventosGtiaEnTramiteBean.setNumEje(rs.getInt(17));
					 eventosGtiaEnTramiteBean.setNomEje(rs.getString(18));
					 eventosGtiaEnTramiteBean.setCodEje(rs.getString(19));
					 eventosGtiaEnTramiteBean.setCodEstado(rs.getString(20));
					 eventosGtiaEnTramiteBean.setGlsEstado(rs.getString(21));
					 eventosGtiaEnTramiteBean.setFlgObs(rs.getString(22));
					 eventosGtiaEnTramiteBean.setGlsObs(rs.getString(23));
					 eventosGtiaEnTramiteBean.setFlgUltEve(rs.getString(24));
					 eventosGtiaEnTramiteBean.setFlgTramTermi(rs.getString(25));
					 eventosGtiaEnTramiteBean.setGlsCssDest(rs.getString(26));
					 eventosGtiaEnTramiteBean.setGlsCssLetras(rs.getString(27));
					 
					 eventosGtiaEnTramiteList.add(eventosGtiaEnTramiteBean);//inserta datos en lista
					 		
				 }
				 
				 rs = (ResultSet)  cs.getObject(6);
				 eventosGtiaEnTramiteResSLAList = (List) new ArrayList();   
				 
				 while (rs.next()) {
	                    
					 EventosGtiaEnTramiteBean eventosGtiaEnTramiteBean = new EventosGtiaEnTramiteBean();
					 
					 eventosGtiaEnTramiteBean.setRolEtapa(rs.getString(1));
					 eventosGtiaEnTramiteBean.setGlsRolEtapa(rs.getString(2));
					 eventosGtiaEnTramiteBean.setNumDiasSLA(rs.getString(3));
					 eventosGtiaEnTramiteBean.setNumDiasRest(rs.getString(4));
					 
					 eventosGtiaEnTramiteResSLAList.add(eventosGtiaEnTramiteBean);
					 		
				 }
				 
				 
				 LogDinamico.debug("Eventos Garantia en tramite: " +  eventosGtiaEnTramiteList.size());
				 	if ( eventosGtiaEnTramiteList.size()==0){
				 		LogDinamico.debug("No se encontraron registros de eventos garantia en tramite" );
					}
				 	
				 	status.setStatus(true);
					status.setObjResul(eventosGtiaEnTramiteList);
					status.setObjResul2(eventosGtiaEnTramiteResSLAList);
					
			} else {
				LogDinamico.debug("ConsultaEventosGtiaEnTramite() ERROR : " + cs.getString(3));
				status.setStatus(false);
			}
			
			BasePeer.commitTransaction(dbConnGrt);
						
		} catch (Exception e) {
			LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite: execute --> Error al consultar: " + e.getMessage());
			e.printStackTrace();
			status.setStatus(false);			
			try {
				BasePeer.rollBackTransaction(dbConnGrt);
			} catch (Exception ex) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite:, error en rollback : " + ex.getMessage());
				status.setStatus(false);
			}
			
			
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				
				if(dbConnGrt != null){
					dbConnGrt.close();
				}
			} catch (SQLException e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			} catch (Exception e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaEventosGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			}
		}
		return status;
	}
	
	
	public StatusBean  ConsultaDocumentosGtiaEnTramite(long numGarantia, long numEvento, String filtro) {
		LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaDocumentosGtiaEnTramite, numGarantia: " + numGarantia +
				", numEVento: "+ numEvento+", filtro: "+filtro);
		
		DBConnection dbConnGrt = null;
		CallableStatement cs  = null;
		OracleResultSet rs  = null;
		List listadoDocumentos = null;		
		StatusBean status = new StatusBean();
		
		try {
			dbConnGrt = BasePeer.beginTransaction(Constants.DB_GRT);			
			String sp = "{call GRT_ADMIN.GRT_SP_LIS_DOCS_GARANTIAS(?,?,?,?,?,?)}"; 
			
			LogDinamico.debug("sp : " + sp);
			cs = dbConnGrt.getConnection().prepareCall(sp);
			
			cs.setLong(1, numGarantia);
			cs.setLong(2, numEvento);
			cs.setString(3, filtro);
			
			cs.registerOutParameter(4, Types.NUMERIC);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR );
			
			cs.executeQuery();
						
			if ( cs.getInt(4) == 0 ){
				
				LogDinamico.debug("ConsultaDocumentosGtiaEnTramite() EXITOSO ");
				rs = (OracleResultSet)  cs.getObject(6);
				listadoDocumentos = (List) new ArrayList();

				while ( rs.next() ){
					
					DocumentosGtiaEnTramiteBean docsGarantia = new DocumentosGtiaEnTramiteBean();

					docsGarantia.setNumGarantia(rs.getInt(1));
					docsGarantia.setFolio(rs.getInt(2));
					docsGarantia.setFecha(rs.getString(3));
					docsGarantia.setGlosa(rs.getString(4));
					docsGarantia.setTipo(rs.getString(5));					
					docsGarantia.setBinFile(rs.getBlob(6));
					docsGarantia.setFileName(rs.getString(7));

					listadoDocumentos.add(docsGarantia);

				}
				
				LogDinamico.debug("Eventos Garantia en tramite: " +  listadoDocumentos.size());
				
				if ( listadoDocumentos.size() == 0 ){					
					LogDinamico.debug("No se encontraron registros de documentos para la garantia " + numGarantia);
				}

				status.setStatus(true);
				status.setObjResul(listadoDocumentos);


			}else {
				LogDinamico.debug("ConsultaDocumentosGtiaEnTramite() ERROR : " + cs.getString(5));
				status.setStatus(false);
			}
			
			BasePeer.commitTransaction(dbConnGrt);
						
		} catch (Exception e) {
			LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaDocumentosGtiaEnTramite: execute --> Error al consultar: " + e.getMessage());
			e.printStackTrace();
			status.setStatus(false);			
			try {
				BasePeer.rollBackTransaction(dbConnGrt);
			} catch (Exception ex) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaDocumentosGtiaEnTramite:, error en rollback : " + ex.getMessage());
				status.setStatus(false);
			}
			
			
		} finally {
			try {
				if (cs != null) {
					cs.close();
				}
				
				if(dbConnGrt != null){
					dbConnGrt.close();
				}
			} catch (SQLException e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaDocumentosGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			} catch (Exception e) {
				LogDinamico.debug("PCO_Frm_GtiasEnTramite.ConsultaDocumentosGtiaEnTramite:, error en dbConnGrt close : " + e.getMessage());
				e.printStackTrace();
				status.setStatus(false);
			}
		}
		return status;
	}
	
	public DBConnection getConexionDb(){
		DBConnection dbConnGrt = null;
		try {
			dbConnGrt = BasePeer.beginTransaction(Constants.DB_GRT);
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return dbConnGrt;
	}

	@Override
	public void loadNew(DataNew arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Key newKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void afterCommit(Key arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeCommit(Key arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkRefresh(RunData arg0, Context arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkValidations(RunData arg0, Context arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyChanges(RunData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setConstantsBasePage() {
		// TODO Auto-generated method stub
		
	}

	
}
