package com.bice.platcom.modules.screens.garantias;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.db.pool.DBConnection;
import org.apache.velocity.context.Context;

import com.bice.platcom.modules.bean.DocumentosGtiaEnTramiteBean;
import com.bice.platcom.modules.bean.StatusBean;
import com.bice.platcom.modules.core.GtiasEnTramiteBDelegate;
import com.bice.platcom.modules.util.Constants;
import com.bice.fogape.mantenedor.soporte.LogDinamico;
import com.sonda.bancos.SONDAframe.modules.screens.SONDAframeScreen;

import oracle.jdbc.driver.OracleResultSet;

public class PCO_Detail_Documents extends SONDAframeScreen {
	
	private List<DocumentosGtiaEnTramiteBean> listaDocumentos = new ArrayList<DocumentosGtiaEnTramiteBean>();
	private GtiasEnTramiteBDelegate gtiasEnTramiteBDelegate = new GtiasEnTramiteBDelegate();
	private StatusBean docsGtiaEnTramite = new StatusBean();

	public PCO_Detail_Documents() {}

	public void doBuildTemplate(RunData data, Context context) throws Exception {
				
		String numGtiaStr = data.getParameters().getString("numGar"," ");
		String numEventoStr = data.getParameters().getString("numEvento"," ");
		String filtro = data.getParameters().getString("filtro"," ");
		String fileName = data.getParameters().getString("fileName"," ");
		String folio = data.getParameters().getString("folio"," ");
		LogDinamico.debug("numGar: " + numGtiaStr);
		LogDinamico.debug("folio archivo a descargar: " + folio);

		HttpServletResponse response = data.getResponse();
		DBConnection conn = null;
		
		if(numGtiaStr != null && numGtiaStr != " "){
			
			docsGtiaEnTramite = gtiasEnTramiteBDelegate.consultaSiHayDocumentos(Long.parseLong(numGtiaStr), Long.parseLong(numEventoStr), filtro);

			if(docsGtiaEnTramite.getStatus()){
				
				LogDinamico.debug("Documentos para la garantia: " + numGtiaStr + ", Evento: "+ numEventoStr + ", Filtro: " +filtro);
	
				this.setListaDocumentos((List)docsGtiaEnTramite.getObjResul() );
				context.put("LISTA_DOCS", this.getListaDocumentos());
				LogDinamico.debug("this.getListaDocumentos.size : " + this.getListaDocumentos().size());
	
			}
		}
		
		
		if(fileName != null && fileName != " "){
			try {

				conn = gtiasEnTramiteBDelegate.getConexion();

				String sql = "SELECT * FROM  TT_BLB where blb_fol  = " + folio; 
				PreparedStatement statement = conn.prepareStatement(sql);

				OracleResultSet result = (OracleResultSet)statement.executeQuery();
				if (result.next()) {
					// gets file name and file blob data
					String tipoFile = result.getString("blb_tpi");
					Blob blob = result.getBlob("blb_blb");

					InputStream in = blob.getBinaryStream();

					String nombreArchivo = "." + tipoFile.toLowerCase(); // extension    
					LogDinamico.debug("nombreArchivo " + nombreArchivo);
					
					String file = fileName + "." + tipoFile.toLowerCase();
					response.setContentType("application/octet-stream");					
	                String headerKey = "Content-Disposition";
	                String headerValue = String.format("attachment; filename=\"%s\"", file);
	                response.setHeader(headerKey, headerValue);
	                
	                
					OutputStream servletoutputstream = response.getOutputStream();
					int BUFFER_SIZE = 1024;
					byte[] buffer = new byte[BUFFER_SIZE];
					int sizeRead = 0;
					while ((sizeRead = in.read(buffer)) >= 0) { //leyendo del host

						servletoutputstream.write(buffer, 0, sizeRead); //escribiendo para el navegador
					}
					
					in.close(); 
					servletoutputstream.close();
			
				}
			} catch (SQLException e) {				
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}finally{
				BasePeer.commitTransaction(conn);
			}
		}

	}

	public List<DocumentosGtiaEnTramiteBean> getListaDocumentos() {
		return listaDocumentos;
	}

	public void setListaDocumentos(List<DocumentosGtiaEnTramiteBean> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}
	

}
